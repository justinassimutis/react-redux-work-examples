import { createSelector } from 'reselect';

const getSongsSelector = ({ musicStore: { songs } }) => songs;

export const getBandsSelector = createSelector(getSongsSelector, songs => {
  const bands = {};
  songs.forEach(el => {
    const { album, band, song } = el;
    const existingBand = bands[band];
    if (existingBand) {
      const existingAlbum = existingBand[album];
      if (existingAlbum) {
        existingAlbum.push(song);
      } else {
        existingBand[album] = [song];
      }
    } else {
      bands[band] = {};
      bands[band][album] = [song];
    }
  });
  return bands;
});

export const getAlbums = createSelector(getBandsSelector, bands => {
  const albums = [];

  Object.keys(bands).forEach(band =>
    Object.keys(bands[band]).forEach(album =>
      albums.push({ band, album, songs: bands[band][album] })
    )
  );

  return albums;
});

export const getAlbumsSorted = createSelector(getAlbums, albums =>
  albums.sort((a, b) => a.band > b.band)
);
