import xhr from 'axios';

const path = '/data.json';

const fetchSongs = () => {
  return xhr.get(path).then(response => response);
};

export default {
  fetchSongs
};
