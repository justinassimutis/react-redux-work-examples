/* global window */
import { applyMiddleware, compose, createStore } from 'redux';
import { browserHistory } from 'react-router';

import { routerMiddleware } from 'react-router-redux';
import createSagaMiddleware from 'redux-saga';

import rootReducer from '../reducers/';
import sagas from '../sagas/';

const sagaMiddleware = createSagaMiddleware();
const routingMiddleware = routerMiddleware(browserHistory);

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default () => {
  const middleware = [sagaMiddleware, routingMiddleware];
  const enhancers = [applyMiddleware(...middleware)];

  const store = createStore(rootReducer, composeEnhancers(...enhancers));

  sagaMiddleware.run(sagas);

  window.store = store;

  return store;
};
