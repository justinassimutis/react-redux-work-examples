import { all } from 'redux-saga/effects';
import api from 'app/api';
import albums from 'app/Albums/sagas/album-saga';

// start the daemons
export default function* root() {
  yield all([albums(api).startWatchers()]);
}
