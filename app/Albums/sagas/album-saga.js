import { takeLatest, put, call } from 'redux-saga/effects';
import { Types, Creators } from '../actions';

export default api => {
  function* songsFetchAttempt() {
    try {
      const resp = yield call(api.fetchSongs);
      yield put(Creators.songsFetchSuccess(resp.data));
    } catch (err) {
      yield put(Creators.songsFetchFail(err));
    }
  }

  function* startWatchers() {
    yield takeLatest(Types.SONGS_FETCH_ATTEMPT, songsFetchAttempt);
  }

  return {
    startWatchers,
    songsFetchAttempt
  };
};
