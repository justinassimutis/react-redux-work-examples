import { createActions } from 'reduxsauce';

export const { Types, Creators } = createActions({
  songsFetchAttempt: null,
  songsFetchSuccess: ['songs'],
  songsFetchFail: ['error']
});
